# Prototipo de eCommerce de Juan Valdez
## < Proyecto propuesto y liderado por [Tita Learning](https://titamedia.com/). />
Este proyecto incluye el uso de HTML, CSS, Sass, Vanilla JavaScript, Node.JS y Express. La página se ha desarrollado principalmente para las vistas de desktop y mobile. 
## Instrucciones para ejecutar el proyecto localmente:
Requisitos:

1. Tener instalado en su sistema NodeJS.
2. Tener instalada la extensión de "**Live Server**" en VSCode.

Dado que en este prototipo de eCommerce se está emulando el consumo de los productos de la tienda desde un servidor local, se deben seguir los siguientes pasos para clonar el directorio y levantar dicho servidor adecuadamente. 

Antes de iniciar, determine la ubicación de la carpeta en la que guardará el repositorio. Luego, desde la línea de comando de su Terminal úbiquese en dicha carpeta.

Ahora, siga los siguientes pasos:

1. Clonar el repositorio: ```git clone https://Rassaster@bitbucket.org/Rassaster/juanvaldez.git```
2. Ubicarse en el directorio del servidor: ```cd juanvaldez/data```
3. Instalación de dependencias en Node.JS: ```npm install```
4. Iniciar servidor de datos: ```npx nodemon server.js```

De esta manera el servidor que aloja el JSON con la información de los productos se encuentra funcionando.

Para finalizar, abra el archivo `index.html` con **Live Server** desde VSC (en Windows: *alt+L, alt+O* || en Mac: *cmd+L, cmd+O*). Deg esta manera podrá navegar la página.