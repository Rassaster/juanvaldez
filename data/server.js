const express = require("express");
const cors = require("cors");
const server = express();
const SERVER_PORT = 3009;

server.use(cors());

const productsJson = require("./info.json")
server.get("/", (req, res) => {
  res.header("Content-Type", "application/json");
  res.send(JSON.stringify(productsJson));
})

server.listen(SERVER_PORT, () => {
  console.log("Estas en la bodega cafetera. Port: " + SERVER_PORT);
})