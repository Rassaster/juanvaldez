const lg = console.log;
let url = "http://localhost:3009/";
const requestStockProducts = async () => {
  let response = await fetch(url);
  let data = await response.json();
  let products = data;
  for (let i = 0; i < products.length; i++) {
    tempStockProds.push(products[i]);
  }
}
let productQtyCart = document.querySelector("#productQtyCart");
let productQtyCartMob = document.querySelector("#productQtyCartMob");
let tempStockProds = [];
export let shoppingCart = [];
const localStorageToShoppingCart = () => {
  if(localStorage.getItem("shoppingCartJV")) {
    let temp = JSON.parse(localStorage.getItem("shoppingCartJV"));
    shoppingCart = temp;
  } else {
    shoppingCart = [];
  }
}
const shoppingCartToLocalStorage = () => {
  localStorage.setItem("shoppingCartJV", JSON.stringify(shoppingCart));
}
const updateCartIcon = () => {
  if (shoppingCart.length > 0) {
    let itemsInCart = shoppingCart.length;
    productQtyCart.style.display = "block";
    productQtyCart.innerText = itemsInCart;
    productQtyCartMob.style.display = "block";
    productQtyCartMob.innerText = itemsInCart;
  } else {
    productQtyCart.style.display = "none";
    productQtyCartMob.style.display = "none";
  }
}
const addToShoppingCart = (productID) => {
  let productInCart = false;
  let indexInCart;
  let desiredProductId = productID;
  tempStockProds.forEach(stockProduct => {
    if (stockProduct.productId === desiredProductId) {
      if(shoppingCart.length === 0) {
        lg("Carro vacío")
        stockProduct.shoppingQty = 1;
        shoppingCart.push(stockProduct);
      } else {
        for (let i = 0; i < shoppingCart.length; i++) {
          if (shoppingCart[i].productId === productID) {
            productInCart = true;
            indexInCart = i;
          }
        }
        if (!productInCart) {
          stockProduct.shoppingQty = 1;
          return shoppingCart.push(stockProduct);
        } else {
          shoppingCart[indexInCart].shoppingQty += 1;
        }
      }
    }
  });
  shoppingCartToLocalStorage();
  updateCartIcon();
}
document.addEventListener("click", event => {
  if(Array.from(event.target.classList).includes("coffee__product__addButton")) {
    addToShoppingCart(event.target.id);
  }
})

// localStorage.clear();
window.onload = function() {
  requestStockProducts();
  localStorageToShoppingCart();
  updateCartIcon();
}