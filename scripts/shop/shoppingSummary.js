const lg = console.log;
let payButton = document.querySelector("#payButton");
let purchaseDoneThanksOverlay = document.querySelector("#purchaseDoneThanks");
let empytCartDisplay = document.querySelector("#empytCartDisplay");
let summaryTable = document.querySelector("#summaryTable");
let subtotalCostDOM = document.querySelector("#subtotalCostDOM");
let shipmentCostDOM = document.querySelector("#shipmentCostDOM");
let discountCostDOM = document.querySelector("#discountCostDOM");
let totalCostDOM = document.querySelector("#totalCostDOM");

let shoppingCartSummary = [];
let summaryCosts = {
  subtotalCostValue: 0,
  shipmentCostValue: 0,
  discountCostValue: 0,
  totalCostValue: 0
}

const setSummaryCostsInDOM = () => {
  totalCostCalculation();
  subtotalCostDOM.innerText = `$${(summaryCosts.subtotalCostValue).toLocaleString()}`;
  shipmentCostDOM.innerText = `$${(summaryCosts.shipmentCostValue).toLocaleString()}`;
  discountCostDOM.innerText = `$${(summaryCosts.discountCostValue).toLocaleString()}`;
  totalCostDOM.innerText = `$${(summaryCosts.totalCostValue).toLocaleString()}`;
}
const subtotalCostCalculation = (qtyInCart, priceBefore) => {
  let productSubtotalCost = qtyInCart * priceBefore;
  summaryCosts.subtotalCostValue += productSubtotalCost;
}
const discountCostCalculation = (qtyInCart, priceAfter, priceBefore) => {
  let productSubtotalCost = qtyInCart * priceBefore;
  let productTotalCost = qtyInCart * priceAfter;
  let discountCost = productTotalCost - productSubtotalCost;
  summaryCosts.discountCostValue += discountCost;
}
const addProductCalculation = (priceAfter, priceBefore) => {
  summaryCosts.subtotalCostValue += priceBefore;
  summaryCosts.discountCostValue += (priceAfter - priceBefore);
}
const substractProductCalculation = (priceAfter, priceBefore) => {
  summaryCosts.subtotalCostValue -= priceBefore;
  summaryCosts.discountCostValue -= (priceAfter - priceBefore);
}
const deleteProductCalculation = (qtyInCart, priceAfter, priceBefore) => {
  summaryCosts.subtotalCostValue -= (qtyInCart * priceBefore);
  summaryCosts.discountCostValue -= (qtyInCart * (priceAfter - priceBefore));
}
const totalCostCalculation = () => {
  summaryCosts.totalCostValue = summaryCosts.subtotalCostValue + summaryCosts.shipmentCostValue + summaryCosts.discountCostValue;
}

const removeFirstThreeChars = (rawIdButton) => {
  return rawIdButton.slice(3);
}
const extractFirstThreeChars = (rawIdButton) => {
  return rawIdButton.slice(0, 3);
}
const obtainIndexInShoppingCart = (productId) => {
  return shoppingCartSummary.findIndex(product => 
    product.productId === productId
  )
}
const cartIsEmpty = () => {
  if (shoppingCartSummary.length === 0) {
    return true;
  } else {
    return false;
  }
}

const displaySuccessPurchase = () => {
  let markup = `
  <div class="overaly__thanksPurchase__message__containter flexCtn">
    <h2 class="overaly__thanksPurchase__message__containter__textThanks">
      Gracias por tu compra!
    </h2 >
    <img class="overaly__thanksPurchase__message__containter__heart" src="assets/svg/iconos_SVGs/gracias_SVGs/corazonGracias-init.svg" alt="Gracias por tu compra!">
    <p class="overaly__thanksPurchase__message__containter__textRedirect">
      En breve volverás a la página de inicio.
    </p>
  </div>
  `
  purchaseDoneThanksOverlay.innerHTML = markup;
  purchaseDoneThanksOverlay.style.width = "100vw";
    purchaseDoneThanksOverlay.style.height = "100%";
}
const displayImposiblePurchase = () => {
  let markup = `
  <div class="overaly__thanksPurchase__message__containter flexCtn">
    <h2 class="overaly__thanksPurchase__message__containter__textThanks">
      No se puede proceder al pago dado que el carrito está vacío.
    </h2 >
    <a href="/index.html#buyPoint" class="overaly__thanksPurchase__message__containter__textRedirect">
      Haz click aquí para ir a la tienda y agregar tus productos.
    </a>
  </div>
  `
  purchaseDoneThanksOverlay.innerHTML = markup;
  purchaseDoneThanksOverlay.style.width = "100vw";
    purchaseDoneThanksOverlay.style.height = "100%";
}

const productsInShoppingCartCallRender = () => {
  for (let i = 0; i < shoppingCartSummary.length; i++) {
    let summaryProductItemContainer = document.createElement("div");
    summaryProductItemContainer.classList.add("purchaseSummary__main__orderCtn__table__item");
    summaryProductItemContainer.classList.add("purchaseSummary__main__orderCtn__table__row");
    summaryProductItemContainer.classList.add("flexCtn");

    let productImg = shoppingCartSummary[i].items[0].images[0].imageUrl;
    let productName = shoppingCartSummary[i].productName;
    let productId = shoppingCartSummary[i].productId;
    let productPrice = shoppingCartSummary[i].items[0].sellers[0].commertialOffer.PriceWithoutDiscount;
    let productQtyCart = shoppingCartSummary[i].shoppingQty;
    let productPriceBefore = shoppingCartSummary[i].items[0].sellers[0].commertialOffer.PriceWithoutDiscount;
    let productPriceAfter = shoppingCartSummary[i].items[0].sellers[0].commertialOffer.ListPrice;
    let productMarkup = 
    `
      <div class=" purchaseSummary__main__orderCtn__table__row__productCell flexCtn">
        <img src="${productImg}" alt="Cafe Empacado">
        <div class="purchaseSummary__main__orderCtn__table__row__productCell__nameCode flexCtn">
          <h3>${productName}</h3>
          <p>COD: ${productId}</p>
        </div>
      </div>
      <div class=" purchaseSummary__main__orderCtn__table__row__priceCell flexCtn">
        <p class="purchaseSummary__main__orderCtn__table__row__priceCell__value">$${productPrice}</p>
      </div>
      <div class=" purchaseSummary__main__orderCtn__table__row__quantityCell flexCtn">
        <p class="purchaseSummary__main__orderCtn__table__row__quantityCell__mobileTitle">Cantidad</p>
        <button id="sub${productId}" class="purchaseSummary__main__orderCtn__table__row__quantityCell__substractButton quantityButton"></button>
        <p id="qty${productId}" class="purchaseSummary__main__orderCtn__table__row__quantityCell__quantityDisplay">${productQtyCart}</p>
        <button id="add${productId}" class="purchaseSummary__main__orderCtn__table__row__quantityCell__addButton quantityButton"></button>
      </div>
      <div class=" purchaseSummary__main__orderCtn__table__row__subtotalCell flexCtn">
        <div class="purchaseSummary__main__orderCtn__table__row__subtotalCell__values flexCtn">
          <p class="purchaseSummary__main__orderCtn__table__row__subtotalCell__values__before">$${productPriceBefore}</p>
          <p class="purchaseSummary__main__orderCtn__table__row__subtotalCell__values__after">$${productPriceAfter}</p>
        </div>
        <button id="del${productId}" class="purchaseSummary__main__orderCtn__table__row__subtotalCell__deleteButton"></button>
      </div>
    `
    summaryProductItemContainer.innerHTML = productMarkup;
    summaryTable.appendChild(summaryProductItemContainer);
    subtotalCostCalculation(productQtyCart, productPriceBefore);
    discountCostCalculation(productQtyCart, productPriceAfter, productPriceBefore)
  }
}
const emptyCartRender = () => {

}
const localStorageToShoppingCart = () => {
  if(localStorage.getItem("shoppingCartJV")) {
    let temp = JSON.parse(localStorage.getItem("shoppingCartJV"));
    shoppingCartSummary = temp;
  } else {
    shoppingCart = [];
  }
}
const shoppingCartToLocalStorage = () => {
  localStorage.setItem("shoppingCartJV", JSON.stringify(shoppingCartSummary));
}


const addProductQty = (productId) => {
  let displayQtyElement = document.getElementById(`qty${productId}`);
  let index = obtainIndexInShoppingCart(productId);
  shoppingCartSummary[index].shoppingQty++;
  displayQtyElement.innerText = shoppingCartSummary[index].shoppingQty;
  shoppingCartToLocalStorage();

  let productPriceBefore = shoppingCartSummary[index].items[0].sellers[0].commertialOffer.PriceWithoutDiscount;
  let productPriceAfter = shoppingCartSummary[index].items[0].sellers[0].commertialOffer.ListPrice;
  addProductCalculation(productPriceAfter, productPriceBefore)
  setSummaryCostsInDOM();
};
const substractProductQty = (productId) => {
  let displayQtyElement = document.getElementById(`qty${productId}`);
  let index = obtainIndexInShoppingCart(productId);
  if (shoppingCartSummary[index].shoppingQty > 1) {
    shoppingCartSummary[index].shoppingQty--;
    displayQtyElement.innerText = shoppingCartSummary[index].shoppingQty;
    
    let productPriceBefore = shoppingCartSummary[index].items[0].sellers[0].commertialOffer.PriceWithoutDiscount;
    let productPriceAfter = shoppingCartSummary[index].items[0].sellers[0].commertialOffer.ListPrice;
    substractProductCalculation(productPriceAfter, productPriceBefore)
  } else if (shoppingCartSummary[index].shoppingQty > 0) {
    deleteProduct(productId);
  }
  shoppingCartToLocalStorage();
  setSummaryCostsInDOM();
};
const deleteProduct = (productId) => {
  let displayQtyElement = document.getElementById(`qty${productId}`);
  let index = obtainIndexInShoppingCart(productId);
  let productRenderDOM = Array.from(document.querySelectorAll(".purchaseSummary__main__orderCtn__table__item"))[index];
  let productQtyCart = shoppingCartSummary[index].shoppingQty;
  let productPriceBefore = shoppingCartSummary[index].items[0].sellers[0].commertialOffer.PriceWithoutDiscount;
  let productPriceAfter = shoppingCartSummary[index].items[0].sellers[0].commertialOffer.ListPrice;

  shoppingCartSummary[index].shoppingQty === 0;
  displayQtyElement.innerText = shoppingCartSummary[index].shoppingQty;

  deleteProductCalculation(productQtyCart, productPriceAfter, productPriceBefore);
  
  shoppingCartSummary.splice(index, 1);
  productRenderDOM.remove();

  if (cartIsEmpty()) {
    empytCartDisplay.style.display = "flex";
  } else {
    empytCartDisplay.style.display = "none";
  }
  shoppingCartToLocalStorage();
  setSummaryCostsInDOM();
};
const emptyCart = () => {

}

// Add Quantity to Product:
document.addEventListener("click", (e) => {
  let actionButton =  extractFirstThreeChars(e.target.id);
  let productId = removeFirstThreeChars(e.target.id);
  if (actionButton === "add") {
    addProductQty(productId);
  }
})
// Substract Quantity to Product:
document.addEventListener("click", (e) => {
  let actionButton =  extractFirstThreeChars(e.target.id);
  let productId = removeFirstThreeChars(e.target.id);
  if (actionButton === "sub") {
    substractProductQty(productId);
  }
  
})
// Delete Product:
document.addEventListener("click", (e) => {
  let actionButton =  extractFirstThreeChars(e.target.id);
  let productId = removeFirstThreeChars(e.target.id);
  if (actionButton === "del") {
    deleteProduct(productId);
  }

})
// Pay Button!
payButton.addEventListener("click", () => {
  if (cartIsEmpty()) {
    displayImposiblePurchase();
  } else {
    displaySuccessPurchase();
    shoppingCartSummary = [];
    shoppingCartToLocalStorage();
    lg(shoppingCartSummary)
    setTimeout(() => {
      window.location.replace("/index.html");
    }, 3800)
  }

})

window.onload = function() {
  localStorageToShoppingCart();
  if (cartIsEmpty()) {
    empytCartDisplay.style.display = "flex";
  } else {
    empytCartDisplay.style.display = "none";
    productsInShoppingCartCallRender();
    setSummaryCostsInDOM();
  }
  lg(shoppingCartSummary);
}