const lg = console.log;
import { createSliderHighlights, createSliderNewReleases } from "./../sliders/slidersMain.js";
const showcaseHighlightedProducts = document.querySelector("#sliderHighlights_wrapper");
const showcaseNewReleasedProducts = document.querySelector("#sliderNewReleases_wrapper");

let url = "http://localhost:3009/";
const productsHighlightedCall = async () => {
  let response = await fetch(url);
  let data = await response.json();
  let products = data;
  lg(products)
  for (let i = 0; i < products.length; i++) {
    let productContainer = document.createElement("div");
    productContainer.classList.add("Rasslider__wrapper__element");
    productContainer.classList.add("section__showcaseHighlights__slider__wrapper__product");
    productContainer.classList.add("coffee__product");
    productContainer.classList.add("flexCtn");
      
    let productImg = products[i].items[0].images[0].imageUrl;
    let productName = products[i].productName;
    let productCategoryRaw = products[i].categories[1];
    let productCategoryRawSlice = productCategoryRaw.slice(1); // Le quito el primer caracter.
    let productCategory = productCategoryRawSlice.slice(0, -1); // Le quito el último caracter.
    let productPriceBefore = products[i].items[0].sellers[0].commertialOffer.PriceWithoutDiscount;
    let productPriceAfter = products[i].items[0].sellers[0].commertialOffer.Price;
    let productId = products[i].productId;
    let productMarkup = 
      `
      <div class="coffee__product__img">
        <div class="coffee__product__img__overlay flexCtn">
          <div class="coffee__product__img__overlay__button flexCtn">
            <p>Vista rápida</p>
          </div>
        </div>
        <img src=${productImg} alt="Imagen de producto.">
        <svg class="coffee__product__img__fav" width="24" height="22" viewBox="0 0 24 22" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path  d="M20.84 3.60999C20.3292 3.099 19.7228 2.69364 19.0554 2.41708C18.3879 2.14052 17.6725 1.99817 16.95 1.99817C16.2275 1.99817 15.5121 2.14052 14.8446 2.41708C14.1772 2.69364 13.5708 3.099 13.06 3.60999L12 4.66999L10.94 3.60999C9.9083 2.5783 8.50903 1.9987 7.05 1.9987C5.59096 1.9987 4.19169 2.5783 3.16 3.60999C2.1283 4.64169 1.54871 6.04096 1.54871 7.49999C1.54871 8.95903 2.1283 10.3583 3.16 11.39L4.22 12.45L12 20.23L19.78 12.45L20.84 11.39C21.351 10.8792 21.7563 10.2728 22.0329 9.60535C22.3095 8.93789 22.4518 8.22248 22.4518 7.49999C22.4518 6.77751 22.3095 6.0621 22.0329 5.39464C21.7563 4.72718 21.351 4.12075 20.84 3.60999V3.60999Z" stroke="#411016" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
        </svg>
        <div class="coffee__product__img__discountMark flexCtn">
          <p>Ahorra 30%</p>
          <p>-30%</p>
        </div>
      </div>
      <div class="coffee__product__name">
        <p>${productName}</p>
      </div>
      <div class="coffee__product__category">
        <p>${productCategory}</p>
      </div>
      <div class="coffee__product__price flexCtn">
        <p class="coffee__product__price__before">$${productPriceBefore}</p>
        <p class="coffee__product__price__full">$${productPriceAfter}</p>
      </div>
      <div id=${productId} class="coffee__product__addButton buttonJV flexCtn">
        Agregar al carrito
      </div>`
    productContainer.innerHTML = productMarkup;
    showcaseHighlightedProducts.appendChild(productContainer);
  }
  // Creación del Slider:
  createSliderHighlights();
  
}
const productsNewReleasedCall = async () => {
  let response = await fetch(url);
  let data = await response.json();
  let products = data;
  lg(products)
  for (let i = 0; i < products.length; i++) {
    let productContainer = document.createElement("div");
    productContainer.classList.add("Rasslider__wrapper__element");
    productContainer.classList.add("section__showcaseNewReleases__slider__wrapper__product");
    productContainer.classList.add("coffee__product");
    productContainer.classList.add("flexCtn");
      
    let productImg = products[i].items[0].images[0].imageUrl;
    let productName = products[i].productName;
    let productCategoryRaw = products[i].categories[1];
    let productCategoryRawSlice = productCategoryRaw.slice(1); // Le quito el primer caracter.
    let productCategory = productCategoryRawSlice.slice(0, -1); // Le quito el último caracter.
    let productPriceBefore = products[i].items[0].sellers[0].commertialOffer.PriceWithoutDiscount;
    let productPriceAfter = products[i].items[0].sellers[0].commertialOffer.Price;
    let productId = products[i].productId;
    let productMarkup = 
      `
      <div class="coffee__product__img">
        <div class="coffee__product__img__overlay flexCtn">
          <div class="coffee__product__img__overlay__button flexCtn">
            <p>Vista rápida</p>
          </div>
        </div>
        <img src=${productImg} alt="Cafe Empacado">
        <svg class="coffee__product__img__fav" width="24" height="22" viewBox="0 0 24 22" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path  d="M20.84 3.60999C20.3292 3.099 19.7228 2.69364 19.0554 2.41708C18.3879 2.14052 17.6725 1.99817 16.95 1.99817C16.2275 1.99817 15.5121 2.14052 14.8446 2.41708C14.1772 2.69364 13.5708 3.099 13.06 3.60999L12 4.66999L10.94 3.60999C9.9083 2.5783 8.50903 1.9987 7.05 1.9987C5.59096 1.9987 4.19169 2.5783 3.16 3.60999C2.1283 4.64169 1.54871 6.04096 1.54871 7.49999C1.54871 8.95903 2.1283 10.3583 3.16 11.39L4.22 12.45L12 20.23L19.78 12.45L20.84 11.39C21.351 10.8792 21.7563 10.2728 22.0329 9.60535C22.3095 8.93789 22.4518 8.22248 22.4518 7.49999C22.4518 6.77751 22.3095 6.0621 22.0329 5.39464C21.7563 4.72718 21.351 4.12075 20.84 3.60999V3.60999Z" stroke="#411016" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
        </svg>
        <div class="coffee__product__img__discountMark flexCtn">
          <p>Ahorra 30%</p>
          <p>-30%</p>
        </div>
      </div>
      <div class="coffee__product__name">
        <p>${productName}</p>
      </div>
      <div class="coffee__product__category">
        <p>${productCategory}</p>
      </div>
      <div class="coffee__product__price flexCtn">
        <p class="coffee__product__price__before">$${productPriceBefore}</p>
        <p class="coffee__product__price__full">$${productPriceAfter}</p>
      </div>
      <div id=${productId} class="coffee__product__addButton buttonJV flexCtn">
        Agregar al carrito
      </div>`
    productContainer.innerHTML = productMarkup;
    showcaseNewReleasedProducts.appendChild(productContainer);
  }
  // Creación del Slider:
  createSliderNewReleases();
}
productsHighlightedCall();
productsNewReleasedCall();